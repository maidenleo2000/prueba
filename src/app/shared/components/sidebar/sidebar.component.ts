import { Component } from '@angular/core';
import { GifsService } from 'src/app/gifs/services/gifs.service';

@Component({
  selector: 'shared-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  constructor(private gifsService: GifsService){}

  //!De esta forma con el getter puedo acceder a los datos desde el HTML, en caso contrario no puedo hacerlo
  get tagsHistory(){
    return this.gifsService.tagsHistory;
  }

  datosBtn(nombre:string){
    console.log(nombre)
    this.gifsService.searchTag(nombre);
  }


}
