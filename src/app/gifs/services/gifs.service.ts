import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchResponse } from '../interfaces/gifs.interfaces';

//?Otra forma de declarar como una CONSTANTE la APIKEY porque sabemos que no va a cambiar
// const GIPHY_API = 'dD7B479WORPMKgBgl5q4gY9QLwj4AyCT';

@Injectable({providedIn: 'root'})
export class GifsService {

  //?Declarando variable para guardar los GIFS
  public gifList: Gif[] = [];

  private _tagsHistory: string[] = [];
  private apiKey:string = 'dD7B479WORPMKgBgl5q4gY9QLwj4AyCT';
  private serviceUrl:string = 'http://api.giphy.com/v1/gifs';

  constructor(private http: HttpClient) {
    this.loadLocalStorage();
   }



  get tagsHistory(){
    return [...this._tagsHistory];
    //?Con el operador SPREAD ... creo una copia del _tagsHistory porque sino se pasan los elementos por referencia. Es para evitar cambios por el usuario
  }

  private organizeHistory(tag:string){
    tag = tag.toLowerCase();

    if (this._tagsHistory.includes(tag)){
      this._tagsHistory = this._tagsHistory.filter((oldtag) => oldtag !== tag) //!Filtro los tags repetidos
    }

    this._tagsHistory.unshift(tag); //?Inserto el nuevo tag al inicio del array

    this._tagsHistory = this.tagsHistory.splice(0,10); //!Corto de 0 a 10 el array y nunca se muestran datos mayores a 10

    this.saveLocalStorage();
  }

  // public searchTag(tag:string):void{
  //   if (tag.length === 0) return; //!Si por teclado no ingreso nada no se hace nada

  //   this.organizeHistory(tag)
  //   // this._tagsHistory.unshift(tag);
  //   // console.log(this.tagsHistory)

  // }

  // public async searchTag(tag:string)Promise<void>{
  //   if (tag.length === 0) return; //!Si por teclado no ingreso nada no se hace nada

  //   this.organizeHistory(tag)
  //   // this._tagsHistory.unshift(tag);
  //   // console.log(this.tagsHistory)
  //   //?Usando FETCH
  //   fetch('http://api.giphy.com/v1/gifs/search?api_key=dD7B479WORPMKgBgl5q4gY9QLwj4AyCT&q=pikachu&limit=10')
  //     .then(resp=>resp.json())
  //     .then(data => console.log(data));
  // }

  private saveLocalStorage():void{
    localStorage.setItem('history', JSON.stringify(this._tagsHistory));
  }

  private loadLocalStorage():void{
    if(!localStorage.getItem('history')) return;

    this._tagsHistory = JSON.parse(localStorage.getItem('history')!);
    this.searchTag(this._tagsHistory[0])
  }

  public  searchTag(tag:string):void{
    if (tag.length === 0) return; //!Si por teclado no ingreso nada no se hace nada

    this.organizeHistory(tag)
    // this._tagsHistory.unshift(tag);
    // console.log(this.tagsHistory)

    //?Usando HTTPClientModule que agrega Angular y basado en suscriber como RXJS

    const params = new HttpParams()
    .set('api_key', this.apiKey)
    .set('limit', '10')
    .set('q', tag)


    //?Sin usar PARAMS
    // this.http.get(`${this.serviceUrl}/search?api_key=dD7B479WORPMKgBgl5q4gY9QLwj4AyCT&q=pikachu&limit=10`).subscribe(resp => {
    //   console.log(resp)

    //?Usando PARAMS
    //!A los observables se les pone en este punto el tipado, sino son GENERICOS
    this.http.get<SearchResponse>(`${this.serviceUrl}/search`, {params: params})
    .subscribe(resp => {
      // console.log(resp.data);
      // console.log(resp.meta);

      this.gifList = resp.data;
      // console.log({gifs: this.gifList});
    })
  }

}
