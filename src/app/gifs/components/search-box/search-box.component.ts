import { Component, ElementRef, ViewChild } from '@angular/core';
import { GifsService } from '../../services/gifs.service';

@Component({
  selector: 'gifs-search-box',
  template: `
    <h5>Buscar</h5>
    <!-- <input type="text" class="form-control" placeholder="Buscar gifs..." (keyup.enter)="searchTag(txtTagInput.value)" SIN USAR ViewChild-->
    <input type="text" class="form-control" placeholder="Buscar gifs..." (keyup.enter)="searchTag()"
    #txtTagInput>
  `
})

export class SearchBoxComponent  {

  @ViewChild('txtTagInput')
  public tagInput!: ElementRef<HTMLInputElement>;


  constructor(private gifsService: GifsService) { }

  // searchTag(newTag:string){
  //   console.log({newTag});
  // } //?SIN USAR ViewChild

  searchTag(){
    const newTag = this.tagInput.nativeElement.value;
    // const newTagDos = this.tagInput;
    // console.log({newTag});
    this.gifsService.searchTag(newTag);
    this.tagInput.nativeElement.value = '';
    console.log(this.gifsService.tagsHistory)
  }

}
